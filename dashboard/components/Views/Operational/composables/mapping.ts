import {
  computed,
  ComputedRef,
  ref,
  watch,
} from "@nuxtjs/composition-api";

// Lodash
import { debounce, throttle } from "lodash";
import concat from "lodash/fp/concat";

// D3.js hand-picking
import * as d3Array from "d3-array";
import * as d3Select from "d3-selection";
import * as d3Shape from "d3-shape";

// Mapbox GL JS & GeoJSON
import mapboxgl, { MapboxGeoJSONFeature } from "mapbox-gl";
import { Feature, FeatureCollection } from "geojson";

// Composables
import useGlobals from "~/composables/globals";
import store from "~/composables/store";
import useFetchData from "~/composables/fetch-data";

interface Cells {
  "cluster_location": {
    id: string
    value: string
    subValue: MapboxGeoJSONFeature
  }
  urgency: {
    value: number
  }
}

interface District {
  "code": string
  "naam": string
  "created_at": string
  "last_modified_at": string | null
  "bounding_box": number[][]
  "id": number
}

// Combine hand-picked modules into new d3 object.
const d3 = Object.assign({}, d3Array, d3Select, d3Shape);

// Urgency colours
const { URGENCY_MAP } = useGlobals();

// Cluster urgency filters.
const filterNone = ["==", ["get", "urgency"], "none"];
const filterLow = ["==", ["get", "urgency"], "low"];
const filterMedium = ["==", ["get", "urgency"], "medium"];
const filterHigh = ["==", ["get", "urgency"], "high"];

// Marker sizes
const THICKNESS = 4;
const RADIUS = 18;

// Store the markers and marker clusters.
const clusters = {
  Zuidoost: [],
  Centrum: [],
  Noord: [],
  Westpoort: [],
  West: [],
  "Nieuw-West": [],
  Zuid: [],
  Oost: [],
};
const markers = {};
let markersOnScreen = {};

// Store the map.
let map = null;

const isMapWithMarkers = () => map && map.getStyle() && map.getSource("markers");

// Scans
let scans = ref([]);

/**
 * Draw markers for unclustered markers on the map with D3.js.
 * @param { object } props - Properties of the marker.
 */
function createSingleMarker (props): HTMLDivElement {
  const div = document.createElement("div");

  const svg = d3.select(div)
    .append("svg")
    .attr("width", RADIUS * 2)
    .attr("height", RADIUS * 2);

  const g = svg.append("g")
    .attr("transform", `translate(${RADIUS}, ${RADIUS})`);

  g.append("circle")
    .attr("r", RADIUS - 2)
    .attr("fill", "rgb(255, 255, 255)")
    .attr("stroke", props.urgencyColour)
    .attr("stroke-width", THICKNESS);

  g.append("circle")
    .attr("r", THICKNESS)
    .attr("fill", props.urgencyColour);

  return div;
}

/**
 * Draw donut charts for clustered markers on the map with D3.js.
 * @param { object } props - Properties of the donut chart marker.
 */
function createDonutChart (props): HTMLDivElement {
  const div = document.createElement("div");

  const data = [
    { type: "none", count: props.none },
    { type: "low", count: props.low },
    { type: "medium", count: props.medium },
    { type: "high", count: props.high },
  ];

  const svg = d3.select(div)
    .append("svg")
    .attr("width", RADIUS * 2)
    .attr("height", RADIUS * 2);

  const g = svg.append("g")
    .attr("transform", `translate(${RADIUS}, ${RADIUS})`);

  const arc = d3.arc()
    .innerRadius(RADIUS - THICKNESS)
    .outerRadius(RADIUS);

  const pie = d3.pie()
    .value(d => d.count)
    .sort(null);

  g.append("circle")
    .attr("r", RADIUS)
    .attr("fill", "rgb(255, 255, 255)");

  g.selectAll("path")
    .data(pie(data.sort((x, y) => d3.ascending(y.count, x.count))))
    .enter()
    .append("path")
    .attr("d", arc)
    .attr("fill", d => URGENCY_MAP.find(u => u.name === d.data.type).color);

  g.append("text")
    .text(props.point_count_abbreviated)
    .attr("text-anchor", "middle")
    .attr("dy", 5)
    .attr("fill", "black")
    .attr("font-size", "14px")
    .attr("font-weight", "bold");

  return div;
}

/**
 * Append an overlay on all markers for easier click event handling.
 * @param { HTMLElement } div - The marker.
 * @param { object } props - Properties of the cluster marker.
 * @param { array } coords - Properties of the cluster marker.
 */
function appendOverlay (div: HTMLDivElement, props: any, coords: any) {
  const overlay = document.createElement("div");

  overlay.classList.add("marker");
  overlay.dataset.coordinates = coords;

  if (!props.cluster) {
    overlay.dataset.clusterId = props.remote_id;
  }

  div.append(overlay);

  return div;
}

// Compute features (markers) for scans.
const scannedFeatures = computed(() => {
  let _scans = [];

  if (store.STATE.loaded.scans) {
    _scans = scans.value.map((row: Cells) => {
      const feature = row.cluster_location.subValue;

      feature.properties.urgency = URGENCY_MAP[Math.floor(row.urgency.value)].name;
      feature.properties.urgencyColour = URGENCY_MAP[Math.floor(row.urgency.value)].color;

      return feature;
    });
  }

  return _scans;
});

// Compute features (markers) for container clusters.
const clusterFeatures = computed((): Feature[] => {
  const district = store.STATE.filters.valueFor.district;

  // Show all container clusters for all districts at once
  // if user has selected "all" in the district filter.
  const reducedClusters = district === "all"
    ? Object.values(clusters).reduce((acc, cur) => concat(acc, cur))
    : clusters[store.STATE.filters.valueFor.district]
  ;

  return reducedClusters.map((cluster) => {
    return {
      type: "Feature",
      properties: {
        address: `${cluster.straatnaam} ${cluster.huisnummer > -1 ? cluster.huisnummer : ""}`,
        urgency: URGENCY_MAP[0].name,
        urgencyColour: URGENCY_MAP[0].color,
      },
      geometry: cluster.geojson,
    };
  });
});

// Merge the container cluster and scan feature sets.
const combinedFeatures = computed(() => {
  const filteredClusterFeatures = clusterFeatures.value.filter((cF) => {
    const foundScan = scannedFeatures.value.find(sF => sF.properties.address === cF.properties.address);
    return foundScan ? cF.properties.address !== foundScan.properties.address : true;
  });

  const scanFeatures = scannedFeatures.value;

  return concat(filteredClusterFeatures, scanFeatures);
});

/**
 * Plot markers when the map is loaded.
 */
function initMarkersAndLayer () {
  const geojson: FeatureCollection = {
    type: "FeatureCollection",
    features: combinedFeatures.value,
  };

  // Abort if map with markers is already existing.
  if (isMapWithMarkers()) {
    return;
  }

  if (!map) {
    return;
  }

  map.addSource("markers", {
    type: "geojson",
    data: geojson,
    cluster: true,
    clusterRadius: 64,
    clusterProperties: {
      none: ["+", ["case", filterNone, 1, 0]],
      low: ["+", ["case", filterLow, 1, 0]],
      medium: ["+", ["case", filterMedium, 1, 0]],
      high: ["+", ["case", filterHigh, 1, 0]],
      locations: ["concat", ["concat", ["get", "address"], ","]],
    },
  });

  // Invisible markers to activate queriable map layer.
  map.addLayer({
    id: "markers",
    type: "circle",
    source: "markers",
    paint: {
      "circle-color": "transparent",
      "circle-radius": 18,
    },
  });
}

/**
 * Update markers when the data is filtered or the map view changes.
 */
function updateMarkers () {
  if (!map || !map.getStyle() || !map.querySourceFeatures("markers")) {
    return;
  }

  const newMarkers = {};
  const renderedFeatures = map.querySourceFeatures("markers");

  renderedFeatures.forEach((feature) => {
    // @ts-ignore
    const coordinates = feature.geometry.coordinates;
    const props = feature.properties;

    let id;
    let marker;

    if (props.cluster) {
      id = props.cluster_id;
      marker = markers[id];

      if (!marker) {
        const el = appendOverlay(createDonutChart(props), props, coordinates);

        marker = markers[id] = new mapboxgl.Marker({
          element: el,
        }).setLngLat(coordinates);
      }
    } else {
      id = props.address;
      marker = markers[id];

      if (!marker) {
        const el = appendOverlay(createSingleMarker(props), props, coordinates);

        marker = markers[id] = new mapboxgl.Marker({
          element: el,
        }).setLngLat(coordinates);
      }
    }

    newMarkers[id] = marker;

    if (!markersOnScreen[id]) {
      marker.addTo(map);
    }
  });

  for (const id in markersOnScreen) {
    if (!newMarkers[id]) {
      markersOnScreen[id].remove();
    }
  }

  markersOnScreen = newMarkers;
}

/**
 * Create or update markers to plot on the map.
 */
function setNewMarkers () {
  if (!isMapWithMarkers()) {
    initMarkersAndLayer();
  } else {
    // @ts-ignore
    map.getSource("markers").setData({
      type: "FeatureCollection",
      features: combinedFeatures.value,
    });

    map.on("sourcedata", throttle((event) => {
      if (event.isSourceLoaded) {
        updateMarkers();
        map.setLayoutProperty("markers", "visibility", "visible");
      }
    }, 500));
  }
}

/**
 * Fetch container cluster locations from the Scan API.
 */
async function fetchClusters () {
  function getClusters (district) {
    try {
      const { DISTRICT_ID_MAP } = useGlobals();
      const { fetchData } = useFetchData();

      return fetchData("clusters", { stadsdeel_id: DISTRICT_ID_MAP[district] });
    } catch (error) {
      console.error(error);
    }
  }

  const district = store.STATE.filters.valueFor.district;

  store.setAsLoaded("clusters", false);

  if (district === "all") {
    const districts = Object.keys(clusters);

    for (const districtName of districts) {
      if (clusters[districtName].length <= 0) {
        const containerClusters: any = await getClusters(districtName);

        clusters[districtName] = await containerClusters.data;
      }
    }
  } else if (clusters[district].length <= 0) {
    const containerClusters: any = await getClusters(district);

    clusters[district] = await containerClusters.data;
  }

  store.setAsLoaded("clusters", true);

  return new Promise((resolve, reject) => {
    try {
      if (store.STATE.loaded.clusters) {
        resolve(clusters);
      }
    } catch (error) {
      reject(error);
    }
  });
}

/**
 * Fly to a district on the map using it's rectangle bounds.
 */
function flyToFiltered (bounds = null) {
  if (!map || !map.getStyle() || !map.getLayer("markers")) {
    return;
  }

  if (!bounds && scannedFeatures.value.length > 0) {
    bounds = new mapboxgl.LngLatBounds();

    scannedFeatures.value.forEach((feature: any) => {
      bounds.extend(feature.geometry.coordinates);
    });
  } else if (!bounds) {
    bounds = map.getMaxBounds();
  }

  map.fitBounds(bounds, {
    padding: 60,
  });
}

// Watch for filter changes and re-render map features.
watch(
  [
    () => store.STATE.filters.valueFor.cluster_location,
    () => store.STATE.filters.valueFor.quarter,
    () => store.STATE.filters.valueFor.area,
    () => store.STATE.filters.valueFor.location_type,
  ],
  debounce(() => {
    const promisedClusters = fetchClusters();
    promisedClusters
      .then(() => flyToFiltered())
      .catch(console.error)
    ;
  }, 500, {
    leading: false,
    trailing: true,
  }),
);

// When the user selects another district fly to it on the map.
watch(
  [
    () => store.STATE.filters.valueFor.district,
    () => store.STATE.filters.valueFor.daterange,
  ],
  throttle(() => {
    const promisedClusters = fetchClusters();
    promisedClusters
      .then(() => {
        updateMarkers();

        if (store.STATE.districts?.filter((district: District) => store.STATE.filters.valueFor.district === district.naam).length > 0) {
          const bounds = store.STATE.districts.filter((district: District) => store.STATE.filters.valueFor.district === district.naam)[0].bounding_box;

          flyToFiltered(bounds);
        }
      })
      .catch(console.error)
    ;
  }, 500),
);

export default function useMapping (
  mapboxMap?: mapboxgl.Map,
  newScans?: ComputedRef<object[]>,
) {
  if (mapboxMap) {
    map = mapboxMap;
  }

  if (newScans) {
    // Ordinary Ref is overwritten by a ComputedRef.
    scans = newScans;

    watch(
      () => store.STATE.loaded.scans,
      (newValue) => {
        if (newValue && scans.value.length > 0) {
          setNewMarkers();
        } else if (isMapWithMarkers()) {
          map.setLayoutProperty("markers", "visibility", "none");
        }
      },
    );

    watch(() => scans.value, debounce(setNewMarkers, 500));
  }

  return {
    setNewMarkers,
    updateMarkers,
    fetchClusters,
    flyToFiltered,
  };
}
