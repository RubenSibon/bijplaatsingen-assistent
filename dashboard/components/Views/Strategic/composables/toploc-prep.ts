// Util libraries
import cloneDeep from "lodash/fp/cloneDeep";
import { AxiosResponse } from "axios";

// Composables
import useGlobals from "~/composables/globals";
import useCache from "~/composables/cache";
import useFetchData from "~/composables/fetch-data";

// Schemas
import topLocDistrictsTableSchema from "~/components/Views/Strategic/schemas/topLocDistrictsTableSchema.json";
import topLoc75TableSchema from "~/components/Views/Strategic/schemas/topLoc75TableSchema.json";

interface TopLocDistrictRow {
  "ranking_new"?: {
    id: string
    value: number | string
    style?: string
  }
  district: {
    id: string
    value: string
  }
  "odk_b": {
    id: string
    value: number
    suffix?: string
    showProgressBar?: boolean
  }
  "odk_scans": {
    id: string
    value: number
  }
}

interface TopLoc75Row {
  "ranking_new": {
    id: string
    value: number | string
    style?: string
  }
  "ranking_old": {
    id: string
    value: number
  }
  "cluster_location": {
    id: string
    value: string
    subValue: {
      id: number
      "remote_id": number
    }
  }
  "odk_b": {
    id: string
    value: number
    suffix?: string
    showProgressBar?: boolean
  }
  "odk_scans": {
    id: string
    value: number
  }
}

const { DISTRICT_ID_MAP } = useGlobals();
const { existsInCache, addToCache, getFromCache } = useCache();
const { fetchData } = useFetchData();

async function fetchTopLocDistricts () {
  const _cacheId = "topLocDistricts";
  const table = cloneDeep(topLocDistrictsTableSchema);

  let done = false;

  try {
    if (existsInCache(_cacheId)) {
      table.filter(tableGroup => tableGroup.tag === "tbody")[0].rows = getFromCache(_cacheId);
    } else {
      const promisedTopLocDistricts: AxiosResponse = await fetchData("cycles/current/descriptives_per_stadsdeel");

      const rawData = promisedTopLocDistricts.data;
      const nullRawData = rawData.filter(row => row.percentage_b <= 0);
      const valueRawData = rawData.filter(row => row.percentage_b > 0);
      const sortedRawData = [...valueRawData.sort((rowA, rowB) => rowA.percentage_b <= rowB.percentage_b ? -1 : 1), ...nullRawData];

      const processedRows: TopLocDistrictRow[] = await sortedRawData.map((data, index): TopLocDistrictRow => {
        const tableRow = cloneDeep(table.find(tableGroup => tableGroup.tag === "tbody").rows[0] as TopLocDistrictRow);

        const rank = data.gemiddeld_aantal_scans > 0 ? index + 1 : "-";

        tableRow.ranking_new.value = rank;
        tableRow.district.value = Object.entries(DISTRICT_ID_MAP).find(entry => entry[1] === data.stadsdeel_id)[0];
        tableRow.odk_b.value = Math.round(data.percentage_b);
        tableRow.odk_scans.value = Math.ceil(data.gemiddeld_aantal_scans);

        return tableRow;
      });

      addToCache(_cacheId, [2, "hours"], processedRows);

      // @ts-ignore
      table.filter(tableGroup => tableGroup.tag === "tbody")[0].rows = processedRows;
    }

    done = true;
  } catch (error) {
    console.error(error);

    done = true;
  }

  return new Promise((resolve, reject) => {
    try {
      if (done) {
        resolve(table);
      }
    } catch (error) {
      reject(error);
    }
  });
}

async function fetchTopLoc75ForDistrict (districtId = null) {
  const _cacheId = `topLoc75ForDistrict${districtId}`;
  const table = cloneDeep(topLoc75TableSchema);

  let done = false;

  try {
    if (existsInCache(_cacheId)) {
      table.filter(tableGroup => tableGroup.tag === "tbody")[0].rows = getFromCache(_cacheId);
    } else {
      const promisedTopLoc75: AxiosResponse = await fetchData("cycles/current", { stadsdeel_id: districtId });

      const processedRows: TopLoc75Row[] = await promisedTopLoc75.data.doellocaties
        .filter(data => data.locatietype.naam === "Probleemlocatie")
        .map((data): TopLoc75Row => {
          const tableRow: TopLoc75Row = cloneDeep(table.find(tableGroup => tableGroup.tag === "tbody").rows[0] as TopLoc75Row);
          const address = `${data.cluster.straatnaam} ${data.cluster.huisnummer > -1 ? data.cluster.huisnummer : ""}`;

          tableRow.ranking_new.value = data.beoordeling;
          tableRow.ranking_old.value = data.beoordeling;
          tableRow.cluster_location.value = address;
          tableRow.cluster_location.subValue = {
            id: data.cluster.id,
            remote_id: data.cluster.remote_id,
          };
          tableRow.odk_b.value = Math.round(data.percentage_b);
          tableRow.odk_scans.value = Math.ceil(data.totaal_aantal_scans);

          return tableRow;
        })
        ;

      addToCache(_cacheId, [12, "hours"], processedRows as TopLoc75Row[]);

      table.filter(tableGroup => tableGroup.tag === "tbody")[0].rows = processedRows as TopLoc75Row[];
    }

    done = true;
  } catch (error) {
    console.error(error);

    done = true;
  }

  return new Promise((resolve, reject) => {
    try {
      if (done) {
        resolve(table);
      }
    } catch (error) {
      reject(error);
    }
  });
}

export default function useTopLocPrep () {
  return {
    topLocDistrictsTableSchema,
    topLoc75TableSchema,
    fetchTopLocDistricts,
    fetchTopLoc75ForDistrict,
  };
};
