// @ts-ignore
import resolveConfig from "tailwindcss/resolveConfig";
import tailwindConfig from "~/tailwind.config.js";

const POST_FILTERABLE_PROPS: string[] = ["location_type", "area", "quarter", "cluster_location"];

const MAPOPTIONS = {
  center: [4.9, 52.37] as number[],
  zoom: 11.5 as number,
  minZoom: 11 as number,
  maxZoom: 18 as number,
  maxBounds: [[4.7, 52.27], [5.1, 52.47]] as number[][],
};

const DISTRICT_ID_MAP = {
  all: null,
  Zuidoost: 1,
  Centrum: 2,
  Noord: 3,
  Westpoort: 4,
  West: 5,
  "Nieuw-West": 6,
  Zuid: 7,
  Oost: 8,
};

const GARBAGE_TYPE_MAP = {
  wood: "hout",
  cardboard: "karton",
  matrasses: "matrassen",
  furniture: "meubels",
  plastic: "plastic",
  carpet: "tapijt",
  bags: "vuilniszakken",
  appliance: "witgoed",
};

// Get global theme colours for markers from tailwind config.
const $tailwind = resolveConfig(tailwindConfig);

const COLORS = $tailwind.theme.colors;

const URGENCY_MAP = [
  {
    name: "none",
    color: $tailwind.theme.colors.gray[400],
  },
  {
    name: "low",
    color: $tailwind.theme.colors.green[800],
  },
  {
    name: "low",
    color: $tailwind.theme.colors.green[800],
  },
  {
    name: "low",
    color: $tailwind.theme.colors.green[800],
  },
  {
    name: "medium",
    color: $tailwind.theme.colors.orange[800],
  },
  {
    name: "high",
    color: $tailwind.theme.colors.red[800],
  },
];

export default function useGlobals () {
  return {
    POST_FILTERABLE_PROPS,
    MAPOPTIONS,
    DISTRICT_ID_MAP,
    GARBAGE_TYPE_MAP,
    COLORS,
    URGENCY_MAP,
  };
}
