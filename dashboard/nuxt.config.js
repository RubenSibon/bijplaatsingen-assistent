import path from "path";
import webpack from "webpack";
import * as pkg from "./package.json";

const cssContentOrder = ["custom-properties", "dollar-variables", "declarations", "at-rules", "rules"];
const cssPropOrder = ["all", "appearance", "box-sizing", "display", "position", "top", "right", "bottom", "left", "float", "clear", "flex", "flex-basis", "flex-direction", "flex-flow", "flex-grow", "flex-shrink", "flex-wrap", "grid", "grid-area", "grid-template", "grid-template-areas", "grid-template-rows", "grid-template-columns", "grid-row", "grid-row-start", "grid-row-end", "grid-column", "grid-column-start", "grid-column-end", "grid-auto-rows", "grid-auto-columns", "grid-auto-flow", "grid-gap", "grid-row-gap", "grid-column-gap", "align-content", "align-items", "align-self", "justify-content", "justify-items", "justify-self", "order", "columns", "column-gap", "column-fill", "column-rule", "column-rule-width", "column-rule-style", "column-rule-color", "column-span", "column-count", "column-width", "backface-visibility", "perspective", "perspective-origin", "transform", "transform-origin", "transform-style", "transition", "transition-delay", "transition-duration", "transition-property", "transition-timing-function", "visibility", "opacity", "mix-blend-mode", "isolation", "z-index", "margin", "margin-top", "margin-right", "margin-bottom", "margin-left", "outline", "outline-offset", "outline-width", "outline-style", "outline-color", "border", "border-top", "border-right", "border-bottom", "border-left", "border-width", "border-top-width", "border-right-width", "border-bottom-width", "border-left-width", "border-style", "border-top-style", "border-right-style", "border-bottom-style", "border-left-style", "border-radius", "border-top-left-radius", "border-top-right-radius", "border-bottom-left-radius", "border-bottom-right-radius", "border-color", "border-top-color", "border-right-color", "border-bottom-color", "border-left-color", "border-image", "border-image-source", "border-image-width", "border-image-outset", "border-image-repeat", "border-image-slice", "box-shadow", "background", "background-attachment", "background-clip", "background-color", "background-image", "background-origin", "background-position", "background-repeat", "background-size", "background-blend-mode", "cursor", "padding", "padding-top", "padding-right", "padding-bottom", "padding-left", "width", "min-width", "max-width", "height", "min-height", "max-height", "overflow", "overflow-x", "overflow-y", "resize", "list-style", "list-style-type", "list-style-position", "list-style-image", "caption-side", "table-layout", "border-collapse", "border-spacing", "empty-cells", "animation", "animation-name", "animation-duration", "animation-timing-function", "animation-delay", "animation-iteration-count", "animation-direction", "animation-fill-mode", "animation-play-state", "vertical-align", "direction", "tab-size", "text-align", "text-align-last", "text-justify", "text-indent", "text-transform", "text-decoration", "text-decoration-color", "text-decoration-line", "text-decoration-style", "text-rendering", "text-shadow", "text-overflow", "line-height", "word-spacing", "letter-spacing", "white-space", "word-break", "word-wrap", "color", "font", "font-family", "font-kerning", "font-size", "font-size-adjust", "font-stretch", "font-weight", "font-smoothing", "osx-font-smoothing", "font-variant", "font-style", "content", "quotes", "counter-reset", "counter-increment", "page-break-before", "page-break-after", "page-break-inside", "pointer-events", "will-change"];

export default {
  /*
  ** Nuxt target
  ** See https://nuxtjs.org/api/configuration-target
  */
  target: "server",

  /*
  ** Headers of the page
  ** See https://nuxtjs.org/api/configuration-head
  */
  head: {
    title: "Bijplaatsingen Assistent",
    meta: [
      { charset: "utf-8" },
      { name: "viewport", content: "width=device-width, initial-scale=1" },
      { hid: "description", name: "description", content: process.env.npm_package_description || "" },
      { name: "msapplication-TileColor", content: "#da532c" },
      { name: "theme-color", content: "#ffffff" },
    ],
    link: [
      { rel: "icon", type: "image/x-icon", href: "/favicon.ico" },
      { rel: "icon", type: "image/png", sizes: "32x32", href: "/favicon-32x32.png" },
      { rel: "icon", type: "image/png", sizes: "16x16", href: "/favicon-16x16.png" },
      { rel: "apple-touch-icon", sizes: "180x180", href: "/apple-touch-icon.png" },
      { rel: "manifest", href: "/site.webmanifest" },
    ],
  },

  /*
  ** Global CSS
  */
  css: [
    "node_modules/mapbox-gl/dist/mapbox-gl.css",
    "assets/css/transitions.pcss",
  ],

  /*
  ** Plugins to load before mounting the App
  ** https://nuxtjs.org/guide/plugins
  */
  plugins: [
    "node_modules/focus-visible/dist/focus-visible.js",
  ],

  /**
   * Nuxt.js router
   */
  router: {
    middleware: [],
  },

  /*
  ** Auto import components
  ** See https://nuxtjs.org/api/configuration-components
  */
  components: true,

  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [
    // Doc: https://composition-api.now.sh/
    "@nuxtjs/composition-api",

    // Doc: https://typescript.nuxtjs.org/guide/setup.html
    [
      "@nuxt/typescript-build",
      {
        ignoreNotFoundWarnings: true,
      },
    ],

    // Doc: https://github.com/nuxt-community/stylelint-module
    "@nuxtjs/stylelint-module",

    // Doc: https://github.com/nuxt-community/nuxt-tailwindcss
    "@nuxtjs/tailwindcss",
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://i18n.nuxtjs.org/
    [
      "nuxt-i18n",
      {
        vueI18nLoader: true,
        strategy: "prefix",
        locales: [
          {
            code: "en",
            iso: "en-GB",
          },
          {
            code: "nl",
            iso: "nl-NL",
          },
        ],
        defaultLocale: "nl",
        seo: false,
        vueI18n: {
          locale: "nl",
          fallbackLocale: "en",
          silentTranslationWarn: true,
        },
      },
    ],

    // Enable various ways to load SVGs.
    "nuxt-svg-loader",

    // Enable scrolling to elements.
    "vue-scrollto/nuxt",
  ],

  typescript: {
    typeCheck: {
      eslint: {
        files: "./**/*.{ts,js,vue}",
      },
    },
  },

  /*
  ** Babel presets
  */
  babel: {
    presets () {
      return [
        [
          "@nuxt/babel-preset-app",
          {
            // List assembled with
            // https://browserl.ist/?q=%3E+1%25+in+NL%2C+last+2+Safari+versions%2C+Firefox+ESR%2C+not+IE+%3C%3D+10%2C+not+ExplorerMobile+%3E+0%2C+not+BlackBerry+%3E+0%2C+not+OperaMini+all%2C+not+OperaMobile+%3E+0%2C+not+dead
            // for > ~90% coverage in the Netherlands.
            targets: pkg.browserslist,
          },
        ],
      ];
    },
  },

  /*
  ** Build configuration
  ** See https://nuxtjs.org/api/configuration-build/
  */
  build: {
    postcss: {
      // Add plugin names as key and arguments as value
      // Install them before as dependencies with npm or yarn
      plugins: {
        // Disable a plugin by passing false as value
        "postcss-import": {},
        tailwindcss: path.resolve(__dirname, "./tailwind.config.js"),
        "postcss-calc": {},
        "postcss-custom-properties": {},
        "postcss-nested": {},
        "postcss-each": {},
        "postcss-extend-rule": {},
        "postcss-short": {},
        "postcss-sorting": {
          "order/order": cssContentOrder,
          "properties-order": cssPropOrder,
        },
        precss: {},
      },
      options: {
        parser: "postcss-html",
      },
    },

    plugins: [
      new webpack.ProvidePlugin({
        mapboxgl: "mapbox-gl",
      }),
    ],
  },
};
